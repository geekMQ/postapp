import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { SubirPage } from '../subir/subir';
import { CargaArchivoProvider, ArchivoSubir } from '../../providers/carga-archivo/carga-archivo';
import { SocialSharing } from '@ionic-native/social-sharing';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  hay:boolean=true;
  items:any[]=[1,2,3,4,5,6];
 // items: Observable<any[]>;
  
  constructor(private modalCTRL: ModalController, public cargar:CargaArchivoProvider,private socialSharing: SocialSharing) {

     // this.items = fireDatabase.list('post').valueChanges();

      //console.log(this.items);

  }


  mostrar_modal(){

    let modal=this.modalCTRL.create(SubirPage);
    modal.present();

  }

  doInfinite2(infiniteScroll) {
    console.log('Begin async operation');

      this.cargar.cargarImagenes().then(
        (res:boolean)=>{
          console.log(res);
          this.hay=res;
          infiniteScroll.complete()

        });
      
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    this.cargar.cargarImagenes().then(
      ( hayMas:boolean )=> {

        console.log(hayMas);
        this.hay = hayMas;

        infiniteScroll.complete();
      }
    );

  }

  share(post:ArchivoSubir){

    // Check if sharing via email is supported
    this.socialSharing.canShareVia("Facebook", post.titulo, post.img, post.img).then(() => {
      this.socialSharing.shareViaFacebook(post.titulo, post.img, post.img)
      .then(()=>{} ) // se pudo compartir
        .catch( ()=>{}) // si sucede un error

    }).catch(() => {
     this.cargar.showToast("No hay facebook");
    });



  }


}
