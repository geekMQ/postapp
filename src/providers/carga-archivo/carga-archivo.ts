import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase';
import {ToastController} from 'ionic-angular';

import 'rxjs/add/operator/map';


@Injectable()
export class CargaArchivoProvider {


  imagenes: ArchivoSubir[] = [];
  lastKey: string = null;

  constructor(public toastCtrl: ToastController, public afDB: AngularFireDatabase ) {

    this.cargar_ultimo_key().subscribe(()=> this.cargarImagenes())
    
  }

  private cargar_ultimo_key(){

   return  this.afDB.list('/post',ref=> ref.orderByKey().limitToLast(1))
    .valueChanges()
    .map( (post:any) =>{
       this.lastKey = post[0].key;
        this.imagenes.push(post[0]);
    })
    

  }

  cargarImagenes(){

    return new Promise( (resolve, reject)=>{

      this.afDB.list('/post',
        ref=> ref.limitToLast(3)
                 .orderByKey()
                 .endAt( this.lastKey )
       ).valueChanges()
        .subscribe(  (posts:any)=>{

          posts.pop();

          if( posts.length == 0 ){
            console.log('Ya no hay más registros');
            resolve(false);
            return;
          }

          this.lastKey = posts[0].key;

          for( let i = posts.length-1;  i >=0; i-- ){
            let post = posts[i];
            this.imagenes.push(post);
          }

          resolve(true);

        });



    });


  }

  cargarImagen_firebase(archivo:ArchivoSubir){
    let post: ArchivoSubir = {
      img: "url",
      titulo: "titulo",
      key: "nombreArchivo"
    };


    let promesa = new Promise((resolve,reject)=>{

      this.showToast("Cargando.....");
      let storeRef = firebase.storage().ref();
      let nombreArchivo :string = new Date().valueOf().toString();

      let upltask : firebase.storage.UploadTask = 
        storeRef.child(`img/${nombreArchivo}`) 
        .putString( archivo.img, 'base64', { contentType: 'image/jpeg' }  );

      upltask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        ()=>{
          //porsentaje de carga
         let percentage= (upltask.snapshot.bytesTransferred / upltask.snapshot.totalBytes) * 100;
         this.showToast(percentage+"",100);
        },
        (err)=>{
          //hubo un error
          this.showToast("Fallo al cargar la imagen..",10000);    
          reject();      
        },
        ()=>{
          //se cargo bien

          upltask.snapshot.ref.getDownloadURL().then((downloadURL)=>{
            let url =downloadURL;                    
            this.crear_post(archivo.titulo,url,nombreArchivo);
          resolve();

          })

        }
        
        )//uptask on end

    });

    return promesa;


  }
  private crear_post( titulo: string, url: string, nombreArchivo:string ){
    let newPostKey = firebase.database().ref().child('post').push().key;

    let post: ArchivoSubir = {
      img:url,
      titulo: titulo,
      key: newPostKey
    };


   
    //this.afDB.list('/post/${newPostKey}').push(post);
    this.afDB.object(`/post/${ newPostKey }`).update(post);
    this.imagenes.push( post );

  }

  showToast(msg:string, time:number=2000){
    const toast= this.toastCtrl.create({
      message:msg,
      duration:time
    });
    toast.present();
  }

}


export interface ArchivoSubir{
  titulo:string;
  img:string;
  key?:string;
}